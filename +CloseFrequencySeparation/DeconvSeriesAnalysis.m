if ~exist('f_TLS_save', 'var')
    CloseFrequencySeparation.Deconvolute;
    close all
end

%% series analysis
[f_TLS_save,I_TLS] = sort(f_TLS_save,1);
[f_0, I_f_0] = sort(squeeze(f_0),1);
phi_0 = squeeze(phi_0);
for k = 1:N_realization
    phi_0(:,k) = phi_0(I_f_0(:,k),k);
    Phi_estimated_esprit(:,k) = Phi_estimated_esprit(I_TLS(:,k),k);
end
Phi_1_estimated_esprit = Phi_estimated_esprit + (1-1/L)*pi*f_TLS_save;

f_c = mean(f_0);

e_fTLS = sort(f_TLS_save,1) - sort(squeeze(f_0),1);
e_fTLS(f_TLS_save==0) = NaN;        % omit detected zero frequencies

phi_1 = squeeze(angle(exp(1j*(phi_0 + (1-1/L)*pi*f_0))));
% e_phiTLS = angle(exp(1j*(Phi_estimated_esprit - phi_0 + (1-1/L)* pi*e_fTLS)));
% e_phiTLS = angle(exp(1j*(Phi_estimated_esprit - phi_0 +  pi*e_fTLS)));
% e_phiTLS = angle(exp(1j*(Phi_estimated_esprit - phi_0 )));
e_phiTLS = angle(exp(1j*(Phi_1_estimated_esprit - phi_1)));

e_phiTLS(f_TLS_save==0) = NaN;
e_phiTLS = reshape(e_phiTLS,2,[]);

e_phi = angle(exp(1j*(Phi_estimated - phi_0)));
% e_phiTLS(f_save==0) = NaN;
e_phi = reshape(e_phi,2,[]);


%%
% As can be seen in figure22 the error analysis contains data points with a
% systematic error. By removing results with two identical detected
% frequencies and by removing 0Hz signals from the analysis, the observed
% errors can be categorized into errors caused by missing frequency
% detection and numerical errors. Both error categiries must be analyzed by
% their number and in magnitude.

addr.distinct = ~(diff(f_TLS_save)==0);
addr.ZeroFreq = (f_TLS_save(1,:)==0);
addr.NaN = isnan(f_TLS_save(1,:));

dPhi = angle(exp(1j*diff(phi_0)));

data_vectors = struct(...
    'f_0', f_0, ...
    'f_c', f_c, ...
    'f_TLS_save', f_TLS_save, ...
    'phi_0', phi_0, ...
    'dPhi', dPhi, ...
    'e_fTLS', e_fTLS, ...
    'e_phiTLS', e_phiTLS, ...
    'e_phi', e_phi, ...
    'sigma_Y_save', sigma_Y_save, ...
    'sigma_Y_ESPRIT_save', sigma_Y_ESPRIT_save, ...
    'SNR_actual', SNR_actual, ...
    'SNR_estimated', sigma_Y_ESPRIT_save, ...
    'SNR_estimated_esprit', sigma_Y_ESPRIT_save);
    
merged_categories = genCats(addr, data_vectors);

simulation_parameters.phi_TLS_RMSE = merged_categories.e_phiTLS.correct.RMSE;
simulation_parameters.phi_RMSE = merged_categories.e_phi.correct.RMSE

%% display error plots:
% Frequency dependent

h_figs = figure(10);
set(h_figs(end), 'Name', 'CloseSuperPosition Error')
plotErrorToDeltaF(...
    merged_categories, 'f_c', 'all', ...
    ['Error for N=' num2str(N_realization) ' realiziations'], 1)

h_figs(end+1) = figure(11);
plotErrorToDeltaF(...
    merged_categories, 'f_c', 'correct', ...
    ['Error for N=' num2str(merged_categories.f_0.correct.N) ' valid realiziations'], 1)


%%
figure(22)
plotErrorToDeltaF(...
    merged_categories, 'dF', 'all', ...
    'Error of all realizations', ...
    0)



figure(23)
plotErrorToDeltaF(...
    merged_categories, 'dF', 'correct', ...
    'Error of valid measurements')

figure(24)
plotErrorToDeltaF(...
    merged_categories, 'f_c', 'correct', ...
    'Error of valid measurements')


figure(27)
plotErrorToDeltaF(...
    merged_categories, 'dF_TLS', 'correct', ...
    'Error of valid measurements')

%%
% Phase dependent
figure(28)
plotErrorToDeltaF(...
    merged_categories, 'dphi_0', 'correct', ...
    'Error of valid measurements')

% figure(34)
% plotErrorToDeltaF(...
%     dphi_0_categorized.invalid, ...
%     e_fTLS_categorized.invalid, ...
%     e_phiTLS_categorized.invalid, ...
%     'Error of invalid measurements')



%% Function definitions
function merged_categories = genCats(addr, y)


mc.f_0 = categorize(y.f_0, addr, 'f', 'Hz');
mc.f_c = categorize(y.f_c.*[1;1], addr, 'f_c', 'Hz');
mc.fTLS = categorize(y.f_TLS_save, addr, 'f', 'Hz');
mc.phi_0 = categorize(y.phi_0/pi, addr, '\Phi', '\pi rad');
mc.dphi_0 = categorize(y.dPhi.*[1;1]/pi, addr, '\Delta\Phi', '\pi rad');
mc.dF = categorize(diff(y.f_0).*[1;1], addr, '\Deltaf', 'Hz');
mc.dF_TLS = categorize(diff(y.f_TLS_save).*[1;1], addr, '\Deltaf_{TLS}', 'Hz');
mc.e_fTLS = categorize(y.e_fTLS, addr, 'frequency error', 'Hz');
mc.e_phiTLS = categorize(y.e_phiTLS/pi, addr, 'phase error',  '\pi rad');
mc.e_phi = categorize(y.e_phi/pi, addr, 'phase error',  '\pi rad');

mc.sigma_Y = categorize(y.sigma_Y_save, addr, 'estimated noise',  '\pi rad');
mc.sigma_Y_TLS = categorize(y.sigma_Y_ESPRIT_save, addr, 'estimated noise (TLS)',  '\pi rad');

mc.SNR = categorize(20*log10(y.SNR_actual), addr, 'input SNR',  'dB');
mc.minSNR = categorize(20*log10(min(y.SNR_actual)).*[1;1], addr, 'minimum input SNR',  'dB');
mc.dSNR = categorize(20*log10(abs(diff(y.SNR_actual))).*[1;1], addr, 'power difference',  'dB');
mc.SNR_est = categorize(20*log10(y.SNR_estimated), addr, 'estimated SNR',  'dB');
mc.SNR_TLS = categorize(20*log10(y.SNR_estimated_esprit), addr, 'estimated SNR (TLS)',  'dB');

merged_categories = mc;

end

function x_categorized = categorize(x, addr, label, unit, range)
if (nargin<3)
    label = '';
end
if nargin<4
    unit = '';
end

addr_distinct = addr.distinct;
addr_ZeroFreq = addr.ZeroFreq;
addr_NaN = addr.NaN;

addr_valid = ~or(addr_ZeroFreq, addr_NaN);

addr_correct = and(addr_distinct,addr_valid);
addr_identical = and(~addr_distinct, addr_valid);
addr_ACDC = and(addr_distinct, addr_ZeroFreq);
addr_DConly = and(~addr_distinct, addr_ZeroFreq);
addr_incorrect = ~addr_correct;

if nargin<7
    r_min = (min(x(1, addr_correct)));
    r_max = max(x(2, addr_correct));
    r_range = r_max - r_min;
    range_order = ceil(log10(r_range))-2;
    r_min = floor(r_min*10^(-range_order))*10^(range_order);
    r_max = ceil(r_max*10^(-range_order))*10^(range_order);
    range = [r_min, r_max];
end

x_categorized = struct(...
    'all', calcRMS(x, label, unit, range), ...
    'correct',calcRMS(x(:, addr_correct), label, unit, range), ...
    'incorrect',calcRMS(x(:, addr_incorrect), label, unit, range), ...
    'identical', calcRMS(x(:, addr_identical ), label, unit, range), ...
    'ACDC', calcRMS(x(:, addr_ACDC), label, unit, range), ...
    'DConly', calcRMS(x(:, addr_DConly), label, unit, range), ...
    'invalid', calcRMS(x(:, addr_NaN), label, unit, range), ...
...%     'mean_failure_model', modelDistribution(mean(x)'.^(-2:2), (1-addr_correct(:)), label), ...
...%     'diff_failure_model', modelDistribution(diff(x)'.^(-2:2), (1-addr_correct(:)), label), ...
    'addr_correct', addr_correct, ...
    'addr_incorrect', addr_incorrect, ...
    'addr_identical', addr_identical, ...
    'addr_ACDC', addr_ACDC, ...
    'addr_DConly', addr_DConly, ...
    'addr_invalid', addr_NaN, ...
    'label', label, ...
    'unit', unit, ...
    'range', range);
end

function x_RMSE = calcRMS(x, label, unit, range)
if (nargin<2)
    label = '';
end
if nargin<3
    unit = '';
end
x_RMSE = struct(...
    'value', x, ...
    'RMSE', sqrt(mean(x(:).'.^2, 'omitnan')), ...
    'MeanSquare', mean(abs(x(:)).'.^2, 'omitnan'), ...
    'Mean', (mean(x(:).'.^2, 'omitnan')), ...
    'Variance', var(x(:)), ...
    'deviation', sqrt( var(x(:), 'omitnan')./length(x(:))), ...
    'label', label, ...
    'unit', unit, ...
    'range', range, ...
    'N', length(x));
end

function plotErrorToDeltaF( y, x_name, category, plotTitle, merged)
if nargin==4
    merged = false;
end
%%

x = getfield(y, x_name);
e_fTLS = getfield(y, 'e_fTLS');
e_phiTLS = getfield(y, 'e_phiTLS');
sigma_estimated = getfield(y, 'sigma_Y_TLS');

x = getfield(x, category);
e_fTLS = getfield(e_fTLS, category);
e_phiTLS = getfield(e_phiTLS, category);
sigma_estimated = getfield(sigma_estimated, category);


%%
if merged
    x.value = x.value(:);
    e_fTLS.value = e_fTLS.value(:);
    e_phiTLS.value = e_phiTLS.value(:);
    sigma_estimated.value = sigma_estimated.value(:);
end
%%

%%

subplot(211)
gensubplot(e_fTLS, plotTitle)

subplot(212)
gensubplot(e_phiTLS);

% subplot(313)
% gensubplot( y.sigma_estimated);

    function gensubplot( y, plotTitle)
        if nargin==1
            plotTitle = ['RMSE: [' ...
                num2str(y.RMSE,3) '\pm' num2str(y.deviation,2) ...
                ']' y.unit ];
        else
            plotTitle = {plotTitle, ...
                ['RMSE: [' ...
                num2str(y.RMSE*1e3,3) '\pm' num2str(y.deviation*1e3,2) ...
                ']m' y.unit] ...
                };
        end
        h = plot((x.value)', y.value', ...
            'LineStyle', 'none', ...
            'Marker', '.');
        if ~merged
            set(h, {'DisplayName'}, {'f_1'; 'f_2'})
            legend()
        end
        xlabel([ x.label ' [' x.unit ']' ])
        ylabel( [ y.label ' [' y.unit ']' ] )
        title(plotTitle)
        grid on
        xlim(x.range)
    end

end

function [data_model] = modelDistribution(X_model, Y, label, unit, range)
C_model = X_model\Y;
Y_model = X_model*C_model;
RMSE_model = sqrt(mean((Y_model - Y).^2, 'omitnan'));
if (nargin<3)
    label = '';
end
if nargin<4
    unit = '';
end
data_model = struct(...
    'value', Y_model, ...
    'C_model', C_model, ...
    'RMSE', RMSE_model, ...
    'deviation', sqrt( var((Y_model(:) - Y(:)), 'omitnan')./length(Y(:))), ...
    'label', label, ...
    'unit', unit, ...
    'N', length(Y(:)));
end

