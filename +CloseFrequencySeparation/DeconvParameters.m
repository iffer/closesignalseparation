%%
% determine frame conditions
N_realization = 1e5;
% N_realization = 10;
rng default

L = 128;
N_fft = 1024;
F_s = L;
% sigma_v = 2^-10;
% -87dB sampling noise (~2^-14.5) determined experimentally
sigma_v_dB = 87;
sigma_v = 10^(-0.05*sigma_v_dB);
PSD_noise = 10*log10(sigma_v^2/L);
psd_yrange = [PSD_noise, 0];

t_vec = (0:L-1)'/L;
f_vec = (0:N_fft-1)'/N_fft*F_s;
w_vec = f_vec/F_s*2*pi;

%%
% create random values
delta_f = F_s/L*0.25;
% delta_f = 0.05;
% delta_f = 0.5;
delta_f = 1;
f_range = [0 0.5]*F_s + [1 -1]*delta_f*0.5;
% f_range = f_range + [5 -5];
% f_range(2) = 5 - 0.5*delta_f;
f_range = [2 8];
% f_range = [1 63];
f_range = [1 10];

amplitude_distribution = 'lin';