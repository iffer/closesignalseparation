mixer = radar.Mixer();

N_positions = 2^10;
pos_min = 2.8;
pos_max = 3.2;
delta_pos = (pos_max - pos_min)/(N_positions-1);
positions = (pos_min:delta_pos:pos_max)';
N_positions = length(positions);
[x1, phi1, d, v] = mixer.getSignal([zeros(N_positions, 2), positions], zeros(N_positions, 3));
[x2, phi2, d2] = mixer.getSignal([zeros(1, 2), median(positions)], zeros(1, 3));


d1 = d.E1(:,1);
d2 = d2.E1(1);

d_actual = d1;
d_actual(:,2) = d2;
y = x1 + x2 + 2^-15*randn(128,4,N_positions);


figure(1);
subplot(211)
plot(x1(:,1,1))
title('ramp 1')
subplot(212)
plot(squeeze(x1(:,1,1:10)))
title('ramp 1-10')

figure(2);
subplot(211)
plot(y(:,1,1))
title('ramp 1')
subplot(212)
plot(squeeze(y(:,1,1:10)))
title('ramp 1-10')

%% 
f_TLS_vec = zeros(5, N_positions);
f_est_vec = zeros(5, N_positions);
f_mean_vec = zeros(3, N_positions);
f_var_vec = zeros(3, N_positions);
for k = 1:N_positions
    [w_TLS, P, w_est] = ...
        analysis.esprit_estimate( ...
        y(:,1,k), 24);
    f_TLS = w_TLS/(2*pi);
    f_est = w_est/(2*pi);
    
    f_TLS_vec(1:P,k) = (f_TLS);
    f_est_vec(1:P,k) = (f_est);
    
    f_mean_vec(1:length(f_mean),k) = f_mean;
    f_var_vec(1:length(f_mean),k) = f_var;
end

d_a_TLS = f_TLS_vec*mixer.maxDistance*2;
d_a_est = f_est_vec*mixer.maxDistance*2;

f_var_vec(f_mean_vec == 0) = NaN;
f_mean_vec(f_mean_vec == 0) = NaN;

d_a_mean = f_mean_vec*mixer.maxDistance*2;
d_a_var = f_var_vec*(mixer.maxDistance*2)^2;

figure(3)
subplot(311)
hold off
plot(positions, d_a_mean(1:2,:))
hold on
plot(positions, d_actual, 'LineStyle','--')
plot(positions, mean(d_actual,2), 'LineStyle',':')
grid minor
xlabel('distance [m]')
ylabel('estimation [m]')
title('esprit TLS distance estimation')
subplot(312)
plot(positions, d_a_mean(1:2,:) - sort(d_actual'))
grid minor
xlabel('distance [m]')
ylabel('estimation error [m]')
title('esprit TLS distance estimation')
subplot(313)
hold off
plot(positions, 10*log10((d_a_mean(1:2,:) - sort(d_actual')).^2), ...
    'DisplayName', 'separate targets')
hold on
% plot(positions, 10*log10(d_a_var))
plot(positions, 10*log10((d_a_mean(1:2,:) - mean(d_actual')).^2), ...
    'DisplayName', 'average target')
grid minor
xlabel('distance [m]')
ylabel('estimation error [m]')
title('esprit TLS distance estimation')
legend 
