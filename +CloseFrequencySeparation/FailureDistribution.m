if ~exist('merged_categories', 'var')
    CloseFrequencySeparation.DeconvSeriesAnalysis;
    close all
end

sigma_dFTLS = merged_categories.dF_TLS.incorrect.RMSE
sigma_dF = merged_categories.dF.incorrect.RMSE

df_0_sorted = sortCategorized(merged_categories.dF);
df_TLS_sorted = sortCategorized(merged_categories.dF_TLS);

% ratio_inv_dF = df_0_sorted.number_inv./df_0_sorted.number_total(~df_0_sorted.addr_all_correct );
% ratio_correct_dF = df_0_sorted.number_correct./df_0_sorted.number_total(df_0_sorted.addr_all_correct );

%% calc density 
dF = 0.01;
N_ratioBins = 100;


FailureRatio.F = calcRatio(merged_categories.f_c, N_ratioBins);
FailureRatio.dF = calcRatio(merged_categories.dF, N_ratioBins);
FailureRatio.dPhi = calcRatio(merged_categories.dphi_0, N_ratioBins);
FailureRatio.SNR = calcRatio(merged_categories.SNR, N_ratioBins);
FailureRatio.minSNR = calcRatio(merged_categories.minSNR, N_ratioBins);
merged_categories.dSNR.range(1) = 0;
FailureRatio.dSNR = calcRatio(merged_categories.dSNR, N_ratioBins);


%% Analyse input dependence of errors
N_ratioBins = 100;

error_dependencies = getDependencies(merged_categories);

%% display figures
clear h_inv
h_inv(1) = figure(4);
set(h_inv(end), 'Name', 'delta F dependency')
plotRatio(FailureRatio.dF, ...
    {'\Deltaf dependency of incorrect measurements'})%, ...
%     ['\sigma_v=' num2str(sigma_v*1e3) 'mV, N_{s}=' num2str(L) ', F_{s}=' num2str(F_s) 'Hz']})

h_inv(end+1) = figure(5);
set(h_inv(end), 'Name', 'delta Phi dependency')
plotRatio(FailureRatio.dPhi, ...
    {'\Delta\Phi dependency of incorrect measurements'})%, ...
%     ['\sigma_v=' num2str(sigma_v*1e3) 'mV, N_{s}=' num2str(L) ', F_{s}=' num2str(F_s) 'Hz']})

h_inv(end+1) = figure(6);
set(h_inv(end), 'Name', 'F dependency')
plotRatio(FailureRatio.F, ...
    {'f dependency of incorrect measurements'})

h_inv(end+1) = figure(7);
set(h_inv(end), 'Name', 'F dependency')
plotRatio(FailureRatio.minSNR, ...
    {'SNR dependency of incorrect measurements'})

h_inv(end+1) = figure(8);
set(h_inv(end), 'Name', 'F dependency')
plotRatio(FailureRatio.dSNR, ...
    {'\DeltaSNR dependency of incorrect measurements'})

%% Disp error distribution
h_inv(end+1) = figure(31);
plotStatisticalError(error_dependencies.f_c, 'f dependency of measurement error')
set(h_inv(end), 'Name', 'e-F dependency')

h_inv(end+1) = figure(32);
plotStatisticalError(error_dependencies.dF, ...
    '\Deltaf dependency of measurement error', 'log')
set(h_inv(end), 'Name', 'e-dF dependency')

h_inv(end+1) = figure(33);
plotStatisticalError(error_dependencies.dphi_0, ...
    '\Delta\Phi dependency of measurement error')
set(h_inv(end), 'Name', 'e-dPhi dependency')

h_inv(end+1) = figure(34);
plotStatisticalError(error_dependencies.e_fTLS, ...
    'e_f dependency of measurement error')
set(h_inv(end), 'Name', 'e-eF dependency')

h_inv(end+1) = figure(35);
plotStatisticalError(error_dependencies.SNR, ...
    'SNR dependency of measurement error', 'log')
set(h_inv(end), 'Name', 'SNR dependency')

%% function definitions
function [ x_count, x_bins] = generateCount(x,x_bins)

% x_min = (floor(min(x)/dx)+1)*dx;
% x_max = ceil(max(x)/dx)*dx;
% x_bins = x_min:dx:x_max;
L = length(x_bins);
dx = mean(diff(x_bins));

x_count_cumulative = zeros(1,L);
for k = 1:L
    x_count_cumulative(k) = sum(x(:)<x_bins(k));
end
x_count = diff([0, x_count_cumulative]);
x_bins = x_bins - 0.5*dx;
end

function data = calcRatio(data_categorized, N)

dx = (data_categorized.range(2) - data_categorized.range(1))/N;
x_bins = [data_categorized.range(1)+dx:dx:data_categorized.range(2)];

[ count.correct, count.dF_correct] = ...
    generateCount(data_categorized.correct.value(1,:), x_bins);
[ count.incorrect, count.dF_incorrect] = ...
    generateCount(data_categorized.incorrect.value(1,:), x_bins);
[ count.identical, count.dF_identical] = ...
    generateCount(data_categorized.identical.value(1,:), x_bins);
[ count.ACDC, count.dF_ACDC] = ...
    generateCount(data_categorized.ACDC.value(1,:), x_bins);
[ count.DConly, count.dF_DConly] = ...
    generateCount(data_categorized.DConly.value(1,:), x_bins);
[ count.invalid, count.dF_invalid] = ...
    generateCount(data_categorized.invalid.value(1,:), x_bins);

[ count.total, count.dF_total] = ...
    generateCount(data_categorized.all.value(1,:), x_bins);

ratio.correct = count.correct ./ count.total;
ratio.incorrect = count.incorrect ./ count.total;
ratio.identical = count.identical ./ count.total;
ratio.ACDC = count.ACDC ./ count.total;
ratio.DConly = count.DConly ./ count.total;
ratio.invalid = count.invalid ./ count.total;
ratio.label = data_categorized.label;
ratio.unit = data_categorized.unit;

count.range = data_categorized.range;

data = struct('ratio', ratio, 'count', count);
end

function plotRatio(data, plt_title)
ratio = data.ratio;
count = data.count;
subplot(211)
hold off
plot(count.dF_correct, ratio.correct, 'DisplayName', 'correct')
hold on
plot(count.dF_incorrect, ratio.incorrect, 'DisplayName', 'incorrect')
plot(count.dF_identical, ratio.identical, 'DisplayName', 'identical')
plot(count.dF_ACDC, ratio.ACDC, 'DisplayName', 'ACDC')
plot(count.dF_DConly, ratio.DConly, ...
    'DisplayName', 'DConly', ...
    'Marker', '.')
plot(count.dF_invalid, ratio.invalid, ...
    'DisplayName', 'invalid', ...
    'Marker', '.')

grid minor
xlabel( [ ratio.label ' [' ratio.unit ']'])
ylabel('ratio')
legend('Location', 'E')
ylim([0 1])
xlim(count.range)
if nargin==3
    title(plt_title)
end
subplot(212)
hold off
semilogy(count.dF_correct, ratio.correct, 'DisplayName', 'correct')
hold on
semilogy(count.dF_incorrect, ratio.incorrect, 'DisplayName', 'incorrect')
semilogy(count.dF_identical, ratio.identical, 'DisplayName', 'identical')
semilogy(count.dF_ACDC, ratio.ACDC, 'DisplayName', 'ACDC')
semilogy(count.dF_DConly, ratio.DConly, ...
    'DisplayName', 'DConly', ...
    'Marker', '.')
semilogy(count.dF_invalid, ratio.invalid, ...
    'DisplayName', 'invalid', ...
    'Marker', '.')

% plot(dF_count.dF_total, dF_count.total, 'DisplayName', 'total')
grid on
xlabel( [ ratio.label ' [' ratio.unit ']'])
ylabel('ratio')
% legend('Location', 'SE')
xlim(count.range)
end

function x_sorted = sortCategorized(x_categorized)

x_sorted.incorrect = sort(x_categorized.incorrect.value(1,:));

[x_sorted.all, I_all] = sort(x_categorized.all.value(1,:));
addr_all_correct = x_categorized.addr_correct(1,:);
% addr_all_correct = addr_all_correct(1,:);
x_sorted.addr_all_correct = addr_all_correct(I_all);
x_sorted.correct = sort(x_categorized.correct.value(1,:));

x_sorted.identical = sort(x_categorized.identical.value(1,:));

x_sorted.number_total = 1:length(x_sorted.all);
x_sorted.number_inv = 1:length(x_sorted.incorrect);
x_sorted.number_correct = 1:length(x_sorted.correct);
x_sorted.number_ident = 1:length(x_sorted.identical);
end

function [a_sorted, b_sorted] = sort2(a, b)
[a_sorted, I] = sort(a)
b_sorted = b(I);
end

function Deps = getDependencies(y)
labels = fieldnames(y);
N_ratioBins = 100;

%% limit error value range
y.e_fTLS.limited = y.e_fTLS.correct;
y.e_fTLS.limited.range = [0 y.e_fTLS.limited.RMSE];
y.e_fTLS.limited.value = abs(y.e_fTLS.limited.value);

%% evaluate
Deps = struct();
for k=1:length(labels)
    try
        if isfield(y.(labels{k}),'limited')
            disp(['Using limited data range for field ''' labels{k} '''.'])
            Deps.(labels{k}) = dependence(y, labels{k}, 'limited', N_ratioBins);
        else
            Deps.(labels{k}) = dependence(y, labels{k}, 'correct', N_ratioBins);
        end
    catch
        warning(['Error calculating dependency for field ''' labels{k} '''.'])
    end
end

end

function d = dependence(y, x_name, category, N)

x = getfield(getfield(y, x_name), category);

e_1 = y.e_fTLS.correct;
e_21 = y.e_phiTLS.correct;
e_22 = y.e_phi.correct;

dx = (x.range(2) - x.range(1))/N;
x_bins = (x.range(1)+dx:dx:x.range(2));

x_data = x.value(:);
y1_data = e_1.value(:);
y21_data = e_21.value(:);
y22_data = e_22.value(:);

X_Cell = num2cell(zeros(N,1));
Y1_Cell = num2cell(zeros(N,1));
Y21_Cell = num2cell(zeros(N,1));
Y22_Cell = num2cell(zeros(N,1));

x_addr_cumulative(:, 1) = x_data<x_bins(1);
x_addr(:,1) = x_addr_cumulative(:, 1);
X_Cell{1} = x_data(x_addr(:,1));
Y1_Cell{1} = y1_data(x_addr(:,1));
Y21_Cell{1} = y21_data(x_addr(:,1));
Y22_Cell{1} = y22_data(x_addr(:,1));

for k=2:N
    x_addr_cumulative(:, k) = (x_data<x_bins(k));
    x_addr(:,k) = and(x_addr_cumulative(:,k), ~x_addr_cumulative(:,k-1));
    X_Cell{k} = x_data(x_addr(:,k));
    Y1_Cell{k} = y1_data(x_addr(:,k));
    Y21_Cell{k} = y21_data(x_addr(:,k));
    Y22_Cell{k} = y22_data(x_addr(:,k));
end

x = getMoments(X_Cell, x);
e_1= getMoments(Y1_Cell, e_1);
e_21= getMoments(Y21_Cell, e_21);
e_22= getMoments(Y22_Cell, e_22);
d = struct('x', x, 'e_1', e_1, 'e_21', e_21, 'e_22', e_22);
end

function M = getMoments(x, x_struct)
L = length(x);
Mean = zeros(L,1);
MeanSquare = zeros(L,1);
Variance = zeros(L,1);

for k = 1:L
    Mean(k) = mean(x{k});
    MeanSquare(k) = mean(abs(x{k}).^2);
    Variance(k) = var(x{k});
end

M = struct(...
        'Mean', Mean, ...
        'MeanSquare', MeanSquare, ...
        'Variance', Variance, ...
        'label', x_struct.label, ...
    'unit', x_struct.unit, ...
    'range', x_struct.range, ...
    'Number', L);
    
end

function plotStatisticalError(s, plt_title, linlog)
arguments
    s (1,1) struct
    plt_title (1,1) string = ""
    linlog (:,1) string {mustBeMember(linlog, ["linear","log"])} = "linear"
end
if strcmp(linlog, 'log')
    lplot = @(x,y)semilogy(x,y);
else
    lplot = @(x,y)plot(x,y);
end

subplot(311)
hold off
lplot(s.x.Mean, sqrt(s.e_1.MeanSquare))
hold on

grid minor
xlabel( [ s.x.label ' [' s.x.unit ']'])
ylabel(['RMSE(f_{ESP}) [' s.e_1.unit ']' ])
% legend('Location', 'E')
% ylim([0 1])
xlim(s.x.range)
if nargin>=2
    title(plt_title)
end
subplot(312)
hold off
lplot(s.x.Mean, sqrt(s.e_22.MeanSquare))
hold on

grid minor
xlabel( [ s.x.label ' [' s.x.unit ']'])
ylabel(['RMSE(\Phi_{}) [' s.e_22.unit ']' ])
% legend('Location', 'E')
% ylim([0 1])
xlim(s.x.range)
subplot(313)
hold off
lplot(s.x.Mean, sqrt(s.e_21.MeanSquare))
hold on

grid minor
xlabel( [ s.x.label ' [' s.x.unit ']'])
ylabel(['RMSE(\Phi_{ESP}) [' s.e_21.unit ']' ])
% legend('Location', 'E')
% ylim([0 1])
xlim(s.x.range)

end