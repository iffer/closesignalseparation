N_spr = 128;    % number of samples per ramp
F_s = N_spr;    % sampling frequency


%%
% use random frequency-phase pair
f_range = [0 0.1]*F_s;
f_x = f_range(2)*rand(1) + [0, 0.1]*randn(1);
phi_0 = (rand(1,2) - 0.5)*2*pi
a_0 = rand(1,2);
a_0 = 1

t_vec = (0:N_spr-1)'/F_s;

x = a_0.*cos(2*pi*t_vec*f_x + phi_0);
y = sum(x,2);

%% Frequency analysis
N_fft = 1024*4;
f_vec = (0:N_fft-1)'/(N_fft)*F_s;
fun_window = @(f_0) sinc((f_vec - f_0)) + sinc((f_vec + f_0));  % sinc function for rect windowed signals
fun_window = @(f_0) ...
    -rectWindowFft((f_vec - f_0)/F_s, N_spr-1).*exp(-1j*2*pi*f_vec/F_s*(N_spr-1)/2);%.*sign(f_0));

X = exp(1j*phi_0).';
X_fft = fft(x, N_fft)/N_spr*2;
Y_fft = fft(y, N_fft)/N_spr*2;

h_windows = fun_window([-f_x, f_x]);

Y_manual = h_windows*[conj(X); X];
X_calc_m = h_windows\Y_manual ;
X_calc_angle_m = reshape(angle(X_calc_m),2,2)
a_calc = abs(X_calc_m)

X_calc = h_windows\Y_fft ;
X_calc_angle = reshape(angle(X_calc),2,2)
% mean(X_calc_angle,2)
% X_calc_angle = X_calc_angle - mean(X_calc_angle,2)
a_calc = abs(X_calc);
%% Deconstruct signals


%% Display results
figure(1)
hold off
plot(t_vec, x, 'DisplayName', 'separate', 'LineStyle', '-.')
hold on
plot(t_vec, y, 'DisplayName', 'sum')
title('time domain oscillation')
legend
xlabel('time [s]')
ylabel('amplitude [V]')
grid

figure(2)
subplot(211)
hold off
plot(f_vec, abs(X_fft), 'DisplayName', 'separate', 'LineStyle', '-.')
hold on
plot(f_vec, abs(Y_fft), 'DisplayName', 'sum')
plot(f_vec, abs(h_windows), 'DisplayName', 'windows', 'LineStyle', ':', 'Marker', '.')
plot(f_vec, abs(Y_manual), 'DisplayName', 'model', 'LineStyle', '--')
title('frequency domain oscillations')
legend
xlabel('frequency [Hz]')
ylabel('|amplitude| [V]')
grid
xlim(f_range)
subplot(212)
hold off
plot(f_vec, unwrap(angle(X_fft))/pi, 'DisplayName', 'separate', 'LineStyle', '-.')
hold on
plot(f_vec, unwrap(angle(Y_fft))/pi, 'DisplayName', 'sum', 'LineStyle', '-')
plot(f_vec, unwrap(angle(h_windows))/pi, 'DisplayName', 'windows', 'LineStyle', ':')
plot(f_vec, unwrap(angle(Y_manual))/pi, 'DisplayName', 'model', 'LineStyle', '--')
% title('frequency domain oscillations')
legend
xlabel('frequency [Hz]')
ylabel('phase [\pi rad]')
grid
xlim(f_range)

%% function definitions
function H = rectWindowFft(x, W)
H = sin(pi*W*x)./(W*sin(pi*x));
H(x==0) = 1/W;
end