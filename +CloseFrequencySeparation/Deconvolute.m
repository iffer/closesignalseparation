%%
% Load simulation parameters
CloseFrequencySeparation.DeconvParameters

%% Initialize simulation vectors/matrices
% 

% f_0 = f_range(1) ...
%     + (f_range(2) - f_range(1))*reshape(rand(1,N_realization), 1, 1, N_realization) ...
%     + sigma_f*[-1, 1].*reshape(randn(1,N_realization), 1, 1, N_realization);
f_0 = f_range(1) ...
    + (f_range(2) - f_range(1))*reshape(rand(1,N_realization), 1, 1, N_realization) ...
    + 0.5*delta_f*[-1, 1].*reshape(rand(1,N_realization), 1, 1, N_realization);
phi_0 = 2*pi*rand([1, 2, N_realization]) - pi;
if strcmp(amplitude_distribution, 'lin')
    a_0 = sqrt(2)*sigma_v + (1 - sqrt(2)*sigma_v)*rand([1, 2, N_realization]);
elseif strcmp(amplitude_distribution, 'log')
    a_0 = sqrt(1)*10.^(-0.05*(sigma_v_dB - 30)*rand([1, 2, N_realization]));
else
    error('amplitude_distribution must be ''lin'' or ''log''.')
end
SNR_actual = sqrt(0.5)*squeeze(a_0)./sigma_v;

simulation_parameters = struct( ...
    'SNR_mean_dB', 20*log10(mean(SNR_actual(:))), ...
    'SNR_range_dB', 20*log10( [min(SNR_actual(:)), max(SNR_actual(:)) ] ), ...
    'amplitude_mean_range_dB', 20*log10( sqrt(0.5)*mean(a_0(:) ) ), ...
    'amplitude_range_range_dB', 20*log10( sqrt(0.5)*[min(a_0(:)), max(a_0(:)) ] ), ...
    'amplitude_distribution', amplitude_distribution, ...
    'f_range', f_range, ...
    'delta_f', delta_f, ...
    'sigma_v_dB', sigma_v_dB);

%%
% create data vectors
x = a_0.*cos(2*pi*f_0.*t_vec + phi_0);
y = sum(x,2) + sigma_v*randn(L,1,N_realization);

%% Frequency domain analysis
%
h_window = hann(L)*2;
X_complex_save = zeros(2,N_realization);
X_complex_esprit_save = zeros(2,N_realization);
sigma_X_save = zeros(2,N_realization);
sigma_Y_save = zeros(2,N_realization);
sigma_Y_ESPRIT_save = zeros(2,N_realization);
f_TLS_save = zeros(2,N_realization);

parfor k = 1:N_realization
    %% compute complex amplitude
    % using the precise signal frequency
%     [X_complex_save(:, k), sigma_Y] = analysis.deConvFD(y(:,:,k), f_0(1,:,k), h_window, L);
    [X_complex_save(:, k),~,sigma_Y] = analysis.deConvTD(y(:,:,k), L, f_0(1,:,k)*2*pi/F_s);
    %%
    % Using the esprit estimate of the angular frequency
    [X_complex_esprit, sigma_X, f_TLS, sigma_Y_esprit] = ...
        analysis.deConvEspritTLS(y(:,:,k), F_s, L);
    %% storing values
    % storing the values might run into errors:
    %
    % >> X_complex_esprit_save(:,k) = X_complex_esprit;
    %    sigma_X_save(:,k) = sigma_X;
    %    f_TLS_save(:,k) = f_TLS;
    % --> 'Unable to perform assignment because the size of the left side
    %     is 1-by-1 and the size of the right side is 2-by-1.'
    % 
    % Using try seems to prevent errors from occuring without impacting the
    % results:
    try
        X_complex_esprit_save(:,k) = X_complex_esprit;
    catch
        warning(['Error in operation ' num2str(k)])
        X_complex_esprit_save(:,k)
        X_complex_esprit
    end
    try
        sigma_X_save(:,k) = sigma_X;
    catch
        warning(['Error in operation ' num2str(k)])
        sigma_X_save(:,k)
        sigma_X
    end
    try
        f_TLS_save(:,k) = f_TLS;
    catch
        warning(['Error in operation ' num2str(k)])
        f_TLS_save(:,k)
        f_TLS
    end
    try 
        sigma_Y_save(:,k) = sigma_Y;
    catch
        warning(['Error in operation ' num2str(k)])
        sigma_Y_save(:,k)
        sigma_Y
    end
    try
        sigma_Y_ESPRIT_save(:,k) = sigma_Y_esprit;
    catch
        warning(['Error in operation ' num2str(k)])
        sigma_Y_ESPRIT_save(:,k)
        sigma_Y_esprit
    end
        
end

k = N_realization;
f_TLS = f_TLS_save(:,k);
%% Post loop computations
Phi_estimated = angle(X_complex_save);
a_estimated = abs(X_complex_save);
SNR_estimated = sqrt(2)*a_estimated./sigma_Y_save;

Phi_estimated_esprit = angle(X_complex_esprit_save);
a_estimated_esprit = abs(X_complex_esprit_save);
SNR_estimated_esprit = sqrt(2)*a_estimated_esprit./sigma_Y_ESPRIT_save;

sigma_Phi = sigma_X_save./a_estimated_esprit;

X_fft = fft(h_window.*x(:,:,k), N_fft)/L*2;
[X_complex, sigma_Y_perfectFreq,  Y, H] = analysis.deConvFD(y(:,:,k), f_0(1,:,k), h_window, N_fft);
[~,sigma_Y, Y_TLS, H_TLS] = analysis.deConvFD(y(:,:,k), f_TLS', h_window);

sigma_Y_perfectFreq
[X_complex, sigma_X_perfectFreq] = analysis.deConvTD(y(:,:,k), L, f_0(1,:,k)/F_s*2*pi)
% [~,sigma_X, Y_TLS, H_TLS] = deConv(y(:,:,k),L, 2*pi* f_TLS');

Y = Y*2;
Y_TLS = Y_TLS*2;
%% Display frequency domaincd

k = N_realization;

h_figs =  figure(1);
f_range_of_interest = mean(f_0(:,:,k)) + 20*delta_f*[-1 1];
plot_freq_phase( ...
    f_vec,Y, X_fft, H, ...
    f_0(:,:,k), f_TLS,  ...
    a_estimated(:,k), a_estimated_esprit(:,k), ...
    Phi_estimated(:,k), Phi_estimated_esprit(:,k), ...
    f_range_of_interest, 'log',  PSD_noise)

h_figs(end+1) = figure(2);
f_range_of_interest = mean(f_0(:,:,k)) + 10*delta_f*[-1 1];
plot_freq_phase( ...
    f_vec,Y, X_fft, H, ...
    f_0(:,:,k), f_TLS, ...
    a_estimated(:,k), a_estimated_esprit(:,k), ...
    Phi_estimated(:,k), Phi_estimated_esprit(:,k), ...
    f_range_of_interest, 'linear')


%% Plot time domain
h_figs(end+1) = figure(101);
set(h_figs(end), 'Name', 'Phase estimation using ESPRIT based frequency')
[y_RMSE_esprit, x_RMSE_esprit] = plotTimeDomain(...
    y(:,:,end), x(:,:,end), ...
    a_estimated_esprit(:,end)', ....
    f_TLS_save(:,end)', ...
    Phi_estimated_esprit(:, end)', ...
    F_s)
h_figs(end+1) = figure(102);
set(h_figs(end), 'Name', 'Phase estimation based on actual signal frequency')
[y_RMSE_actual, x_RMSE_actual] = plotTimeDomain(...
    y(:,:,end), x(:,:,end), ...
    abs(X_complex)', ...
    f_0(:,:,end), ...
    angle(X_complex)', ...
    F_s)
% sigma_X_save(:,end)


%% Function definitions





function [y_RMSE, x_RMSE] = plotTimeDomain(y,x,a, f, Phi, F_s, title_txt)
arguments
    y (:,1) double
    x (:,2) double
    a (1,2) double
    f (1,2) double
    Phi (1,2) double
    F_s (1,1) double
    title_txt (:,1) string = ''
end
L = length(y);

x_est = a.*cos(2*pi/F_s*f.*(0:L-1)' + Phi);
y_est = sum(x_est,2);
e_y = y(:,:,end) - y_est;
e_x = x(:,:,end) - x_est;

x_RMSE = sqrt(mean(e_x.^2));
y_RMSE = sqrt(mean(e_y.^2));

subplot(311)
hold off
plot(y(:,:,end), 'DisplayName', 'y')
hold on
% plot(x(:,:,end), 'DisplayName', 'x', 'LineStyle', '-.')
% plot(x_est, 'DisplayName', 'x_{est}', 'LineStyle', 'none', 'Marker', '.')
plot(y_est, 'DisplayName', 'y_{est}', 'LineStyle', 'none', 'Marker', '.')
grid minor
legend
title({title_txt, 'Observed and estimated signal'})
xlabel 'sample'
ylabel 'amplitude'
subplot(312)
hold off
plot(x(:,:,end), 'DisplayName', 'x')
hold on
% plot(x(:,:,end), 'DisplayName', 'x', 'LineStyle', '-.')
plot(x_est, 'DisplayName', 'x_{est}', 'LineStyle', 'none', 'Marker', '.')
% plot(y_est, 'DisplayName', 'y_{est}', 'LineStyle', 'none', 'Marker', '.')
grid minor
legend
title('actual and estimated signals')
xlabel 'sample'
ylabel 'amplitude'
subplot(313)
hold off
plot(e_y, 'DisplayName', 'e_y')
hold on
plot(e_x, 'DisplayName', 'e_x', 'LineStyle', '-.')
% plot(a_estimated_esprit(:,end)'.*cos(2*pi/F_s*f_TLS_save(:,end)'.*(0:L)' + Phi_estimated_esprit(:, end)'), 'DisplayName', 'x_{est}')
grid minor
legend
title('Estimation error')
xlabel 'sample'
ylabel 'amplitude'
end

function plot_freq_phase( ...
    f_vec,Y, X_fft, H, ...
    f_0, f_TLS, ...
    a_estimated, a_estimated_esprit, ...
    Phi_estimated, Phi_estimated_esprit, ...
    f_range_of_interest, ...
    linlog, PSD_noise)
arguments
    f_vec (:,1) double
    Y (:,1) double
    X_fft (:,2) double
    H (:,:) double
    f_0 (1,2) double
    f_TLS (2,1) double
    a_estimated (2,1) double
    a_estimated_esprit (2,1) double
    Phi_estimated
    Phi_estimated_esprit
    f_range_of_interest (1,2) double = [0 f_vec(end/2)]
    linlog (:,1) string {mustBeMember(linlog, ["linear","log"])} = "linear"
    PSD_noise (1,1) double = 0
end

Y_angle = angle(Y);
X_fft_angle = (angle(X_fft));
H_angle = (angle(H));

if strcmp(linlog, 'log')
    set(gcf, 'Name', 'CloseSuperPosition LogScale')
    Y = 20*log10(abs(Y));
    X_fft = 20*log10(abs(X_fft));
    H = 20*log10(abs(H));
    a_estimated = 20*log10(a_estimated);
    a_estimated_esprit = 20*log10(a_estimated_esprit);
    if PSD_noise == 0
        PSD_noise = mean(Y);
    end
    psd_yrange = [PSD_noise, 0];
    amplitude_label = 'power [dB]';
else
    set(gcf, 'Name', 'CloseSuperPosition LinScale')
    Y = abs(Y);
    X_fft = (abs(X_fft));
    H = (abs(H));
    a_estimated = abs(a_estimated);
    a_estimated_esprit = abs(a_estimated_esprit);
    amplitude_label = 'amplitude [V]';
end

f_range_of_interest(1) = max([0 f_range_of_interest(1)]);
f_range_of_interest(2) = min([f_vec(end/2) f_range_of_interest(2)]);

subplot(211)
hold off
plot(f_vec, Y, 'DisplayName', 'Y')
hold on
plot(f_vec, X_fft, ...
    'DisplayName', 'X', ...
    'LineStyle', ':')
plot(f_vec, H, ...
    'DisplayName', 'H_k', ...
    'LineStyle', '--')
if strcmp(linlog, 'log')
    stem(f_0, a_estimated, ...
        'DisplayName', 'X_{est}', ...
        'LineStyle', 'none')
    stem(f_TLS.*[1;1], a_estimated_esprit, ...
        'DisplayName', 'X_{esprit}', ...
        'LineStyle', 'none', ...
        'Marker', 'x')
else
    stem(f_0, a_estimated, ...
        'DisplayName', 'X_{est}')
    stem(f_TLS.*[1;1], a_estimated_esprit, ...
        'DisplayName', 'X_{esprit}', ...
        'LineStyle', '-', ...
        'Marker', 'x')
end
grid on
legend
xlabel('frequency [Hz]')
ylabel(amplitude_label)
title('Superposition of signals with little frequency separation')
% xlim(f_range)

xlim(f_range_of_interest)
if strcmp(linlog, 'log')
    ylim(psd_yrange)
end
subplot(212)
hold off
plot(f_vec, Y_angle, 'DisplayName', 'Y')
hold on
plot(f_vec, X_fft_angle, ...
    'DisplayName', 'X', ...
    'LineStyle', ':')
plot(f_vec, H_angle, ...
    'DisplayName', 'H_k', ...
    'LineStyle', '--')
stem(f_0, Phi_estimated, 'DisplayName', 'X_{est}')
stem(f_TLS.*[1;1], (Phi_estimated_esprit), ...
    'DisplayName', 'X_{esprit}', ...
    'LineStyle', 'none', ...
    'Marker', 'x')
grid on
legend
xlabel('frequency [Hz]')
ylabel('phase [rad]')
% xlim(f_range)
xlim(f_range_of_interest)
end
