function [X, sigma_X, f, RMSE] = deConvEspritTLS(y, F_sampling, L)
%DECONVESPRITTLS Detect frequency and phase of harmonic signals
%   First a TLS variant of ESPRIT is used to determine the frequencies f of
%   a harmonicsignal y. With the signal frequencies the corresponding
%   complex signal amplitude comtaining amplitude and phase are detected.
%
%   Usage:
%   [X, sigma_X, f, RMSE] = deConvEspritTLS(y, F_sampling, L)
%       y           time domain signal
%       F_sampling  sampling frequency
%       L           Length of signal vector
%       X           complex signal amplitude
%       sigma_X     uncertainty of X
%       f           signal frequency values
%       RMSE        root mean square error of the input signal compared to
%                   a harmonic signal with the frequency f and the
%                   amplitude and phase of X.



% [w_TLS, w_est, P, w_TLS_var] = ...
%         CloseFrequencySeparation.esprit_estimate(y_k,24);

[w] = analysis.esprit_estimate_TLS(y,24);
%     [w_TLS, w_TLS_var] = ...
%         CloseFrequencySeparation.findPairs(w_TLS);
if not(isempty(w))
    f = w/(2*pi)*F_sampling;
%     [X_complex_esprit, sigma_Y,~,~,RMSE] = analysis.deConvFD(y_k, f_TLS, 1, L);
%     sigma_X = sigma_Y/(L/2 - length(X_complex_esprit) );
    [X, sigma_X, RMSE] = analysis.deConvTD(y, L, w');
else
    f = [NaN; NaN];
    X = [NaN; NaN];
    sigma_X = [0; 0];
    RMSE = sqrt(mean(y.^2));
end
end
