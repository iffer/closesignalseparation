function [X, sigma_X, sigma_Y] = deConvTD(y, L, w)
%% DECONVTD Determines complex signal vectors of a time series signal
% DeconvTD(y, L, w) determines complex signal vectors X corresponding to 
% harmonic oscillations, that are part of the signal vector y. The
% harmonic oscillations' angular frequencies must be contained in w.
% Usage:
%   [X, sigma_X, sigma_Y] = deConvTD(y, L, w)
%   X: complex signal amplitudes
y_t = y;
addr_zero = (w==0);
w_nz = w(~addr_zero);
w_z = w(addr_zero);
n_zero = sum(addr_zero);
N_w = sum(~addr_zero);

%% least squares
% A_w = exp(1j*(0:L-1)'*[ w_nz, -w_nz]);
phi_nz = (0:L-1)'*w_nz;
A_w = exp(1j*[ phi_nz, -phi_nz]);
% A_w = [cos(phi_nz), sin(phi_nz), ones(L,1)];
% A_w = [cos(phi_nz), sin(phi_nz)];
% A_w = cos([phi_nz, (phi_nz + 0.5*pi)]);
% A_inv = (A_w'*A_w)\A_w';
% X_est = A_inv*y_t;

%% total least squares
X_est = analysis.tls(A_w,y_t);

%%    
X = X_est((1:N_w),:) + conj(X_est( (N_w+1:2*N_w),:) );
% X = X_est((1:N_w),:) - 1j*X_est( (N_w+1:2*N_w),:) ;

e_y = y_t - A_w*X_est;
sigma_Y = sqrt(mean(abs(e_y).^2));
% sigma_X = sqrt(mean(diag(A_inv*A_inv')))*sigma_Y;
sigma_X = sigma_Y/(length(y_t) - 2*N_w );
% sigma_X = sqrt(mean(diag(A_inv*e_y*e_y'*A_inv')));

end