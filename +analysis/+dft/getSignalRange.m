function range_k = getSignalRange(P_y)
if size(P_y,2)>=2
%     disp('too many dimensions')
    P_y = mean(P_y,2);
end
L = length(P_y);
[P_max, k_max] = max(P_y);

dP = diff(P_y);
k_left = 1;
for k=k_max-1:-1:1
    if dP(k)<0
        k_left = k+1;
        break
    end
end
if k_left==1
    P_min = P_y(k_left);
else
    P_min = -1;
%     dP(
end

k_right = L;
for k=k_max+1:L
    if or(dP(k)>0, P_y(k-1)<P_min)
        k_right = k;
        break
    end
end


range_k = (k_left:k_right)';


end