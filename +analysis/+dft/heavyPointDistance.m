function [Y_target,Distance_hp,deviation_hp, Y2] = ...
    heavyPointDistance(y, N, distance_k)
%HEAVYPOINTDISTANCE Summary of this function goes here
%   Detailed explanation goes here
[P_y, Y, Y2] = analysis.dft.absPhase(y, N, N.k_maximum_range);
    
%     Y2_lp = Y2_lp*a_lp(1) + Y2*a_lp(2);
    %% Find main target lobe
    k_range = analysis.dft.getSignalRange(P_y);
    P_y = P_y(k_range,:);
    Y = Y(k_range,:);
    %% normalize psd accross mixer.antennas
    P_mixer.antennas = sum(P_y);
    P_y_normed = P_y./P_mixer.antennas;
%     Y_normed = Y./sqrt(P_mixer.antennas);
    
    %% get target phase
    % this consists of the absolute value and the phase, that allows
    % detecting small distance changes
%     Y_l(l,:) = sum(P_y_normed.*Y_normed);
    Y_target = sum(P_y_normed.*Y)';
    
    
    %% get target distance
    Distance_hp= sum(P_y_normed.*distance_k(k_range))';
    deviation_hp = sqrt(sum((P_y_normed.*distance_k(k_range) - Distance_hp'/length(k_range)).^2))';

end

