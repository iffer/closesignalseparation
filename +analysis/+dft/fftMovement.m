function [Y_2dv, target] = fftMovement(Y_2dCache, window_v, N, DimensionalRange, P_min)
%FFTMOVEMENT Summary of this function goes here
%   Detailed explanation goes here

Y_2dv = fft(Y_2dCache.*window_v, N.fft_v);
Y_2dv = [Y_2dv(end/2+1:end,:,:,:); Y_2dv(1:end/2,:,:,:)];

target = analysis.dft.findNtargets(...
    Y_2dv, ...
    N.targets, DimensionalRange, P_min, ...
    'Interactive', 0, ...
    'EnablePlots', 0);
end

