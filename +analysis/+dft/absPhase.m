function [P_y, y, Y_fft2] = absPhase(y, N, k_maximum_range)
%% ABSPHASE(y, N, k_maximum_range) returns PSD in mW
% P_y: PSD in mW referenced to 50 Ohm
% y: complex voltage vector of each antenna
% Y_fft2: complex voltage vector with range and directional resolution

% N.fft = 128*4;
if ndims(y)==3
    [L, nRxmixer.antennas, nMultiplex] = size(y);
    y = reshape(y, size(y,1), []);
else
    nMultiplex = 1;
    [L,nRxmixer.antennas] = size(y);
end
    

window = hann(L);
window2 = hann(nRxmixer.antennas+2);

% window = 1;
% window=kaiser(length(y),nRxmixer.antennas);
window = window/mean(window);
Y_fft_a = fft(y.*window, N.fft_d)/L;
Y_fft = [Y_fft_a(1,:); Y_fft_a(2:end/2, :)*2];
if nargin==3
Y_fft = Y_fft(1:k_maximum_range,:);
N.fft_d = k_maximum_range*2;
end

    P_y = Y_fft.*conj(Y_fft)* 20;   % P = U^2/R=
    y = (Y_fft);
    
    %% directional fft
    Y_fft = Y_fft./sqrt(sum(P_y));
    Y_fft = reshape(Y_fft, N.fft_d/2, nRxmixer.antennas, nMultiplex);
    
    Y_fft2 = zeros(N.fft_d/2, N.fft_Delta, nMultiplex);
    for n = 1:nMultiplex
        Y_fft2(:,:,n) = fft( ...
            [zeros(N.fft_d/2,1), ...
            Y_fft(:,:,n), ...
            zeros(N.fft_d/2,1)]' .*window2, ...
            N.fft_Delta)'/4;
    end
    Y_fft2 = [Y_fft2(:,end/2+1:end,:), Y_fft2(:,1:end/2,:)];
end
