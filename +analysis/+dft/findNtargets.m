function [target] = findNtargets(Y,N_targets, DimensionalRange, P_min, varargin)
%FINDNTARGETS(Y,N, x, y, z, P_min) Summary of this function goes here
%   Detailed explanation goes here
global P_max_global P_min_global
x = DimensionalRange.speed;
y = DimensionalRange.distance;
z = DimensionalRange.delta;
% Y = Y_2dv;
global EnablePlots Interactive
clear k
P_min_global = P_min;
EnablePlots = 0;
Interactive = 0;
for k=1:2:length(varargin)-1
    switch varargin{k}
        case 'EnablePlots'
            EnablePlots = varargin{k+1};
        case 'Interactive'
            Interactive = varargin{k+1};
        otherwise
            warnign(['Unknown parameter ''' varargin{k+1} ''''])
    end
end
clear k

Y_shape = size(Y);
% Y = reshape(Y, [Y_shape(1:2), 0.5*Y_shape(3), 2]);
P = Y.*conj(Y);

% P = mean(reshape(P, [Y_shape(1:2), 0.5*Y_shape(3), 2]),4);
P = mean(P,4);



x = reshape(x,[],1);
y = reshape(y,[],1);
z = reshape(z,[],1);
%% find targets
P_remaining = P;
% targets = {};
% P_min_dB = mean(10*log10(P), 'all')+20
% P_min = 10^(0.1*P_min_dB);

[P_max, I_max_lin] = max(P(:));
if P_max == 0
    P_max_global = 1;
else
    P_max_global = P_max;
end

[target, P_remaining] = MeasureTarget(x,y,z,P_remaining,Y, I_max_lin);
k_target = 2;
if Interactive
    disp('Press key to find next target.')
    pause()
end

while(k_target<=N_targets)
    [P_max, I_max_lin] = max(P_remaining(:));
    if P_max > P_min_global
        [target(k_target), P_remaining] = MeasureTarget(x,y,z,P_remaining,Y, I_max_lin);
        if Interactive
            disp('Press key to find next target.')
            pause()
        end
        k_target = k_target + 1;
    else
        target(k_target) = emptyTarget();
        k_target = k_target+1;

    end
% targets(k) = target;
end
if Interactive
    disp('Done.')
end
% end

%% function definitions
function [target, P_remaining] = MeasureTarget(x,y,z,P,Y, I_max_lin)
Y_shape = size(P);
%% find target indices

[I_x,I_y,I_z] = ind2sub(Y_shape,I_max_lin);
P_max = P(I_x, I_y, I_z);

%% get 1D circular wrapped target spectra and axes
P_x = reshape(P(:, I_y, I_z), [], 1);
P_y = reshape(P(I_x, :, I_z), [], 1);
P_z = reshape(P(I_x, I_y, :), [], 1);

P_x_circ = repeat(P_x);
P_z_circ = repeat(P_z);
x_circ = repeatAxis(x);
z_circ = repeatAxis(z);
I_x_circ = I_x + 0.5*Y_shape(1);
I_z_circ = I_z + 0.5*Y_shape(3);

%% get 1D heavypoints and deviations
x_range_circ = getRange(P_x_circ, I_x_circ, Y_shape(1)*2);
y_range = getRange(P_y, I_y, Y_shape(2));
z_range_circ = getRange(P_z_circ, I_z_circ, Y_shape(3)*2);
x_range = mod(x_range_circ - 1 - 0.5*Y_shape(1), Y_shape(1) ) + 1;
z_range = mod(z_range_circ - 1 - 0.5*Y_shape(3), Y_shape(3) ) + 1;
[x_heavy, var_x, Px_cumulated] = ...
    HeavyPoint(P_x_circ(x_range_circ), x_circ(x_range_circ));
[y_heavy, var_y, Py_cumulated] = ...
    HeavyPoint(P_y(y_range), y(y_range));
[z_heavy, var_z, Pz_cumulated] = ...
    HeavyPoint(P_z_circ(z_range_circ), z_circ(z_range_circ));

heavyPoints = [x_heavy, y_heavy, z_heavy];
variances = [var_x, var_y, var_z];
deviations = sqrt(variances);

%% get target prominence

P_peak_cumulated = sum( P( x_range, y_range, z_range ), 'all');
P_all_cumulated = sum(P, 'all');
TargetEnergyRatio = P_peak_cumulated / P_all_cumulated;

y_angle = zeros(2,1);
for k_tx=1:2
y_angle(k_tx) = ...
    sum( ...
        Y(x_range, y_range, z_range,k_tx).*P(x_range, y_range, z_range), ...
        'all') ...
    / P_peak_cumulated;
end
dPhi_y = angle(y_angle(2)*conj(y_angle(1)));

%% get Target volume
%  idxs = (...
%      reshape((((1:length(x))-I_x)/length(x_range)).^2, [], 1, 1) ...
%      + reshape((((1:length(y))-I_x)/length(y_range)).^2, 1, [], 1) ...
%      + reshape((((1:length(z))-I_x)/length(z_range)).^2 , 1, 1, []) ...
%      ) < 1;
P_remaining = P;
P_remaining( x_range(2:end-1), y_range(2:end-1), z_range(2:end-1) ) = 0;
P_remaining( x_range, y_range, z_range ) = 0;

target = struct(...
    'heavyPoint', heavyPoints, ...
    'deviation', deviations, ...
    'P_cumulated', P_peak_cumulated, ...
    'P_max', P_max, ...
    'TargetEnergyRatio', TargetEnergyRatio, ...
    'dPhi_y', dPhi_y, ...
    'Y_complex', y_angle, ...
    'Px_cumulated', Px_cumulated, ...
    'Py_cumulated', Py_cumulated, ...
    'Pz_cumulated', Pz_cumulated);
Idx_range = struct(...
    'x', x_range, ...
    'y', y_range, ...
    'z', z_range);

%% plot target ellipses
if EnablePlots
    h = plotMesh(x,y,z,I_x,I_y,I_z,P, 20);
    plotTargetEllipses(h, heavyPoints, deviations)
    % h25 = plotMesh(x,y,z,I_x,I_y,I_z,P_remaining, 25);
    %% plot target spectra
    plotTargetSpectra(x, y, z, P_x, P_y, P_z, ...
        x_range_circ, y_range, z_range_circ, ...
        target)
end

end

function target = emptyTarget()
target = struct(...
    'heavyPoint', NaN*[1 1 1], ...
    'deviation', NaN*[1 1 1], ...
    'P_cumulated', 0, ...
    'P_max', 0, ...
    'TargetEnergyRatio', 0, ...
    'dPhi_y', NaN, ...
    'Y_complex', NaN, ...
    'Px_cumulated', 0, ...
    'Py_cumulated', 0, ...
    'Pz_cumulated', 0);
end


function range_k = getRange(P_y, k_max, L)
dP = diff([P_y; P_y(1,:)]);
k_left = 1;
for k_sample=k_max-1:-1:1
    if dP(k_sample)<0
        k_left = k_sample+1;
        break
    end
end
if k_left==1
    P_min = P_y(k_left);
else
    P_min = 0;
%     dP(
end

k_right = L;
for k_sample=k_max+1:L
idx_dP = k_sample;
idx_Py = k_sample-1;
% idx_dP = mod(k-1,L)+1
% idx_Py = mod(k-2,L)+1;
    if or(dP(idx_dP)>0, P_y(idx_Py)<=P_min)
        k_right = min(k_sample,L);
        break
    end
end

range_k = (k_left:k_right)';
end

function [x_heavy, var_x, P_cumulated] = HeavyPoint(P, x)
P_cumulated = sum(P);
P_normed = P/P_cumulated;
x_heavy = sum(P_normed.*x);
var_x = sum(P_normed.*(x - x_heavy).^2);
end

function x_circular = repeat(x)
x_circular = [x(end/2+1:end); x; x(1:end/2)];
end

function x_circular = repeatAxis(x)
x_circular = [x(end/2+1:end) + 2*x(1); x; x(1:end/2) - 2*x(1)];
end

function [x,y] = ellipse(x0, y0, ax, by, N)
phi = (0:N)/N* 2 * pi;
x = x0 + ax*sin(phi);
y = y0 + by*cos(phi);
end

function h = plotMesh(x,y,z,I_x,I_y,I_z,P, pltNum)
h.f = figure(pltNum);
h.xy = subplot(231);
hold off
imagesc(y,x,10*log10(P(:,:,I_z)));
hold on
title('Power distribution')
xlabel('distance [m]')
ylabel('speed [m/s]')
set(h.xy, ...
    'CLim', 10*log10([P_min_global, P_max_global]), ...
    'CLimMode', 'manual')
h.xz = subplot(232);
hold off
imagesc(z*10^3,x,10*log10(squeeze(P(:,I_y,:))));
hold on
title('Power distribution')
xlabel('\Delta d [mm]')
ylabel('speed [m/s]')
set(h.xz, ...
    'CLim', 10*log10([P_min_global, P_max_global]), ...
    'CLimMode', 'manual')
h.yz = subplot(233);
hold off
imagesc(z*1e3,y,10*log10(squeeze(P(I_x,:,:))));
hold on
title('Power distribution')
xlabel('\Delta d [mm]')
ylabel('distance [m]')
% colorbar('Location','eastoutside')
set(h.yz, ...
    'CLim', 10*log10([P_min_global, P_max_global]), ...
    'CLimMode', 'manual')

h.z = subplot(234);
hold off
imagesc(y,x,10*log10(mean(P,3)));
hold on
title('Power distribution')
xlabel('distance [m]')
ylabel('speed [m/s]')
set(h.z, ...
    'CLim', 10*log10([P_min_global, P_max_global]), ...
    'CLimMode', 'manual')
h.y = subplot(235);
hold off
imagesc(z*10^3,x,10*log10(squeeze(mean(P,2))));
hold on
title('Power distribution')
xlabel('\Delta d [mm]')
ylabel('speed [m/s]')
set(h.y, ...
    'CLim', 10*log10([P_min_global, P_max_global]), ...
    'CLimMode', 'manual')
h.x = subplot(236);
hold off
imagesc(z*1e3,y,10*log10(squeeze(mean(P,1))));
hold on
title('Power distribution')
xlabel('\Delta d [mm]')
ylabel('distance [m]')
% colorbar('Location','eastoutside')
set(h.x, ...
    'CLim', 10*log10([P_min_global, P_max_global]), ...
    'CLimMode', 'manual')
end

function plotTargetEllipses(h, heavyPoints, deviations)
N_ellipses = 32;
[x_ellipse, y_ellipse] = ...
    ellipse( ...
        heavyPoints(1), heavyPoints(2), ...
        deviations(1), deviations(2), ...
        N_ellipses);
plot(h.xy, y_ellipse, x_ellipse, 'r')
[x_ellipse, y_ellipse] = ...
    ellipse( ...
        heavyPoints(1), heavyPoints(3), ...
        deviations(1), deviations(3), ...
        N_ellipses);
plot(h.xz,y_ellipse*1e3, x_ellipse, 'r')
[x_ellipse, y_ellipse] = ...
    ellipse( ...
        heavyPoints(2), heavyPoints(3), ...
        deviations(2), deviations(3), ...
        N_ellipses);
plot(h.yz, y_ellipse*1e3, x_ellipse, 'r')
end

function plotTargetSpectra(x, y, z, P_x, P_y, P_z, ...
    x_range_circ, y_range, z_range_circ, target)

Y_shape = [length(x), length(y), length(z)];
x_circ = repeatAxis(x);
z_circ = repeatAxis(z);
P_x_circ = repeat(P_x);
P_z_circ = repeat(P_z);

[P_max, I_y] = max(P_y);
[~, I_x] = max(P_x);
[~, I_z] = max(P_z);

P_cumulated = target.P_cumulated;
heavyPoints = target.heavyPoint;
std = target.deviation;
Px_cumulated = sum(P_x_circ(x_range_circ));
Py_cumulated = sum(P_y(y_range));
Pz_cumulated = sum(P_z_circ(z_range_circ));

yrange = ...
    [floor(log10(P_min_global)-0.2)*10, 10*ceil(log10(P_max_global)+0.2)];
%%
figure(21)
subplot(311)
hold off
plot(x_circ,10*log10(P_x_circ), ...
    'DisplayName','PSD')
hold on
stem(x(I_x), 10*log10(P_max), ...
    'LineStyle', 'none', ...
    'DisplayName', 'max')
plot(x_circ(x_range_circ), 10*log10(P_x_circ(x_range_circ)), ...
    'Marker','.', ...
    'DisplayName', 'range')
stem(heavyPoints(1) + 1*[-1 0 1]*std(1), 10*log10(P_max*[0.5 1 0.5]), ...
    '.', ...
    'DisplayName', 'x_{heavy} (\pm\sigma)')
fillOuterRange(x, x_circ, 1)
grid minor
title 'speed'
xlim([min(x_circ), max(x_circ)])
ylim(yrange)
xlabel('speed [m/s]')
ylabel('power [dBm]')
legend('Box', 'off') 

subplot(312)
hold off
plot(y,10*log10(P_y), ...
    'DisplayName','PSD')
hold on
stem(y(I_y), 10*log10(P_max), ...
    'LineStyle', 'none', ...
    'DisplayName', 'max')
plot(y(y_range), 10*log10(P_y(y_range)), ...
    'Marker','.', ...
    'DisplayName', 'range')
stem(heavyPoints(2) + 1*[-1 0 1]*std(2), 10*log10(P_max*[0.5 1 0.5]), ...
    '.', ...
    'DisplayName', 'x_{heavy} (\pm\sigma)')
title 'distance'
xlim([min(y), max(y)])
ylim(yrange)
grid minor
xlabel('distance [m]')
ylabel('power [dBm]')
legend('Box', 'off') 

subplot(313)
hold off
plot(1e3*z_circ,10*log10(P_z_circ), ...
    'DisplayName','PSD')
hold on
stem(1e3*z(I_z), 10*log10(P_max), ...
    'LineStyle', 'none', ...
    'DisplayName', 'max')
plot(1e3*z_circ(z_range_circ), 10*log10(P_z_circ(z_range_circ)), ...
    'Marker', '.', ...
    'DisplayName', 'range')
stem( ...
    1e3*(heavyPoints(3) + 1*[-1 0 1]*std(3)), ...
    10*log10(P_max*[0.5 1 0.5]), ...
    '.', ...
    'DisplayName', 'x_{heavy} (\pm\sigma)')
fillOuterRange(1e3*z, z_circ*1e3, 3)

title 'path length difference'
ylabel('power [dBm]')
xlabel '\Delta d [mm]'
xlim(1e3*[min(z_circ), max(z_circ)])
ylim(yrange)
grid minor
legend('Box', 'off') 

end

function fillOuterRange(x_axis, x_circ, dim)

h = fill(...
    [0.5*(x_axis(end) + x_circ(1.5*Y_shape(dim)+1)), ...
        x_circ(end), x_circ(end), ...
        0.5*(x_axis(end) + x_circ(1.5*Y_shape(dim)+1))], ...
    [-100, -100, 30, 30], [1,1,1]*0.5, ...
    'FaceAlpha', 0.5, ...
    'LineStyle', 'none');
h(2) = fill(...
    [x_circ(1),0.5*(x_circ(0.5*Y_shape(dim)) + x_axis(1)), 0.5*(x_circ(0.5*Y_shape(dim)) + x_axis(1)), x_circ(1)], ...
    [-100, -100, 30, 30], [1,1,1]*0.5, ...
    'FaceAlpha', 0.5, ...
    'LineStyle', 'none');

h(1).Annotation.LegendInformation.IconDisplayStyle = 'off';
h(2).Annotation.LegendInformation.IconDisplayStyle = 'off';
end


end