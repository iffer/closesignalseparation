function [f_mean, f_var] = findPairs(f_mat)
f_pos = f_mat(f_mat>0);
f_neg = f_mat(f_mat<0);
if sum(f_mat==0)
    f_zero = 0;
else
    f_zero = [];
end

% n_pairs = min([length(f_pos), length(f_neg)]);

[~,idx_pos] = min((f_pos + f_neg').^2);
[~,idx_neg] = min((f_pos(idx_pos)' + f_neg).^2);


f_mean = 0.5*(f_pos(idx_pos) - f_neg(idx_neg));
f_var = (f_pos(idx_pos) + f_neg(idx_neg)).^2;

[f_mean, idx] = sort(f_mean);
f_var = f_var(idx);

f_mean = [f_zero; f_mean];
f_var = [f_zero; f_var];

end