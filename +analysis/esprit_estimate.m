function [w_est, P, w_est_var] = esprit_estimate(x, M)

% sigma_v =  2^-14;

% y = y + sigma_v*randn(size(y));

% x = y(:,1,1) ;
% x = sum(y(:,:,1,l),2);
% x = x + sigma_v*randn(size(x));

% P = 6;


% M = L�nge des Zeitfensters
% M = 24;

% Generieren der NxM Datenmatrix X
N = length(x) - M + 1;
X = zeros(N,M) ;
for n = 1:M
    X(:,n) = x((1:N)+ (M-n)) ;
end


%%
% Berechne die Singul�rwertzerlegung der Datenmatrix
[L,S,U] = svd(X) ;

S_dB = 20*log10(diag(S));
S_dB_mean = mean(S_dB);
P = sum(S_dB > S_dB_mean + 3 ) + 0;

% Signalunterraum ((M-l) mal P)-Matrix der rechten Singul�rvektoren
Us = U(:,1:P);

% Splitten des Unterraums in zwei Unterr�ume, entsprechend der nicht
% verschobenen bzw. verschobenen Versionen
U1 = Us(1:(M-1),:);
U2 = Us(2:M,:);
% Aufl�sen nach der Rotation zwischen diesen Unterr�umen auf (LS�Methode)
Psi = U2\U1;
%%	

% L�se f�r die Originalrotaion auf, die f�r die Originaldaten ben�tigt wird
phi = eig(Psi); % Eigenwerte von Psi

% Estimate frequencies
w_est = angle(phi);	% Frequenzsch�tzung

% % d_a_est = f_est*mixer.maxDistance*2
% % d_a_est(abs(d_a_est)<10)
% 
% %% Total Least Squares ESPRIT
% [LL, SS, UU] = svd([U1, U2]);
% UU12 = UU(1:P, (P+1):(2*P));
% UU22 = UU((P+1):(2*P), (P+1):(2*P));
% Psi = -UU12*inv(UU22);
% phi = eig(Psi);
% w_TLS = angle((phi));
% % d_a_TLS = f_TLS*mixer.maxDistance*2

%% find pairs
[w_est, w_est_var] = analysis.findPairs(w_est);
P = length(w_est);