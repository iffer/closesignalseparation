function [X_complex, sigma_Y, Y, H, RMSE] = deConvFD(y, f_0, window, nfft)
L = length(y);
if not(isempty(f_0))
    if nargin<3
        window=1;
    end
    if nargin<4
        nfft = 2^nextpow2(length(y));
    end
    Y = fft(window.*y, nfft)/L;
    H = DeConvPattern(f_0, window, nfft, L);
% %     H_inv = (H'*H)\H';
% % %     X_complex = H_inv*Y(1:end/2,1);
% %     X_complex = H_inv*Y;
    X_complex = analysis.tls(H, Y);
    H = H(1:end,:);
    
    RMSE = sqrt(mean(abs(Y - H*X_complex).^2));
    %     X_complex = H\Y(1:end/2,1);
    sigma_Y = exp(mean(log(abs(Y))));
    %     e_Y = H*X_complex - Y(1:end/2,1);
    %     P_X = diag(H_inv*e_Y*e_Y'*H_inv')
%     sigma_X = sqrt((diag(H_inv*H_inv')))*sigma_Y;

    X_complex = 2*X_complex(1:end/2);% + conj( X_complex(end/2+1:end) );
    sigma_X = sigma_Y/(L/2 - length(X_complex) );
    sigma_Phi = sigma_X./abs(X_complex);
else
    X_complex = NaN;
    Y = [];
    H = [];
%     RMSE = sqrt(mean(abs(Y(1:end/2,1) ).^2));
end
end

function h = DeConvPattern(f_0, window, N_fft, L)

addr_zero = (f_0==0);
f_nz = f_0(~addr_zero);

x = exp(2j*pi*[f_nz(:)', -f_nz(:)'].*(0:L-1)'/L);
h = fft(window.*x,N_fft)/L;
% h = h(1:0.5*N_fft,:);
end