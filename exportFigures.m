function exportFigures(h, simulation_parameters)


%% determine valid figures
N_fig = 0;
for k=1:length(h)
    if isa(h(k),'matlab.ui.Figure')
        N_fig = N_fig + 1;
        fig(N_fig) = h(k);
        if isempty(h(k).Name)
            warning(['Figure ' num2str(h(k).Number) ' has empty ''Name'' property.'])
        end
    end
end
timestamp = char(...
    datetime('now',...
    'TimeZone','local',...
    'Format','yyyy-MM-dd''T''HHmmssXXXX'));
% timestamp = '';
path = fullfile(pwd, 'figures');
mkdir(path, timestamp)
path = fullfile(path, timestamp)
for k=1:N_fig
    filename = [num2str(fig(k).Number) '_' fig(k).Name];
    filename(filename==' ') = '_';
    set(fig(k), 'Renderer', 'painters');
    saveas(fig(k),...
        fullfile(path, [filename '.eps']),...
        'epsc')
    saveas(fig(k),...
        fullfile(path, [filename '.png']),...
        'png')
end

%% store simulation_parameters
if nargin==2
    savestruct(simulation_parameters, fullfile(path, 'parameters.tex') )
end

end

function savestruct(YourStruct, name)
fid = fopen(name, 'w');
data = struct2cell(YourStruct);
labels = fieldnames(YourStruct);
for k=1:length(data)
    data_cell(k) = {num2str(data{k})};%mat2cell(data, ones(1,nrow), ncol);  %break by rows
    lbl = labels{k};
    lbl(lbl=='_') = ' ';
    labels{k} = lbl;
end
labels_cell = labels(:);
maxlen = max( cellfun(@length, labels_cell) );
rowfmt = ['%-', sprintf('%d', maxlen), 's & ', '%s  \\\\'];

% rowfmt_nl = ['%-', sprintf('%d', maxlen), 's & ', '%s \\\\\n'];
labeled_data_cell = [labels_cell.'; data_cell(:).'] ;  %transpose it!

lastline = labeled_data_cell(:,end);
labeled_data_cell = labeled_data_cell(:,1:end-1);

fprintf(fid, [rowfmt '\n'], labeled_data_cell{:});
fprintf(fid, rowfmt, lastline{:});
fclose(fid);
end